# copernicus sentinel 3 data parser python


A lot of people using Copernicus data have trouble retrieving it, especially the raw number data.

Here you can find a parser in Python which creates a file that is made of tuples that consists of latitude, longitude + any other data specified 
and downloaded from an .nc file.

Here is an example that uses an .nc file downloaded from Copernicus Open Access Hub (https://scihub.copernicus.eu/dhus/#/home) with information
about temperature from Sentinel 3. On this page  you can add filters to find the data relevant to you,
and even specify a general location of your wanted data.

The program which can help you see the data inside the .nc file and plot it, is Panoply (https://www.giss.nasa.gov/tools/panoply/).

I hope this helps.
